// Copyright (c) 2021 Gitpod GmbH. All rights reserved.
// Licensed under the GNU Affero General Public License (AGPL).
// See License-AGPL.txt in the project root for license information.

package ide

const (
	CodeIDEImage                = "ide/code"
	CodeIDEImageStableVersion   = "commit-c5beb9076768e6ab3717db6e74bdab4b53304ad9" // stable version that will be updated manually on demand
	CodeDesktopIDEImage         = "ide/code-desktop"
	CodeDesktopInsidersIDEImage = "ide/code-desktop-insiders"
	IntelliJDesktopIDEImage     = "ide/intellij"
	GoLandDesktopIdeImage       = "ide/goland"
	PyCharmDesktopIdeImage      = "ide/pycharm"
	PhpStormDesktopIdeImage     = "ide/phpstorm"
)
